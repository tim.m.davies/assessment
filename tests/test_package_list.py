

class TestListNoPackages(object):

    def test_check_empty_list_returned(self, api):
        response = api.get('packages')
        assert response.json() == []

    def test_check_response_code(self, api):
        response = api.get('packages')
        assert response.status_code == 200


def test_list_single_package_check_returned(api):
    package = api.create_package()
    response = api.get('packages')
    assert response.status_code == 200
    assert response.json() == [package]


def test_list_multiple_packages_check_returned(api):
    packages = [api.create_package() for _ in range(3)]
    response = api.get('packages')
    assert response.status_code == 200
    assert response.json() == packages