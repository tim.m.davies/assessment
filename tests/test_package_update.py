import pytest


@pytest.mark.xfail
def test_update_name(api):
    package = api.create_package()
    payload = {
        'name': 'Barry',
        'description': package['description'],
        'productResources': []
    }
    response = api.put('packages/{}'.format(package['id']), json=payload)
    assert response.status_code == 200
    assert response.json()['name'] == 'Barry'
    assert api.packages == [response.json()]


@pytest.mark.xfail
def test_update_add_products(api):
    products = api.products
    package = api.create_package()
    payload = {
        'name': package['name'],
        'description': package['description'],
        'productResources': [{'id': products[0]['id']}]
    }
    response = api.put('packages/{}'.format(package['id']), json=payload)
    assert response.status_code == 200
    assert response.json()['productResources'] == [products[0]]


@pytest.mark.xfail
def test_update_change_products(api):
    products = api.products
    package = api.create_package(products=[products[0]['id']])
    payload = {
        'name': package['name'],
        'description': package['description'],
        'productResources': [{'id': products[1]['id']}]
    }
    response = api.put('packages/{}'.format(package['id']), json=payload)
    assert response.status_code == 200
    assert response.json()['productResources'] == [products[1]]