import pytest
import requests


def pytest_addoption(parser):
    parser.addoption(
        '--api-root',
        action='store',
        default='http://localhost:50080/api/v1/',
        help='Root of packages API to use'
    )


class PackagesAPI(object):

    def __init__(self, api_root):
        self.api_root = api_root
        self.session = requests.Session()

    def get(self, path, **kwargs):
        full_path = '{}{}'.format(self.api_root, path)
        return self.session.get(full_path, **kwargs)

    def post(self, path, **kwargs):
        full_path = '{}{}'.format(self.api_root, path)
        return self.session.post(full_path, **kwargs)

    def delete(self, path, **kwargs):
        full_path = '{}{}'.format(self.api_root, path)
        return self.session.delete(full_path, **kwargs)

    def put(self, path, **kwargs):
        full_path = '{}{}'.format(self.api_root, path)
        return self.session.put(full_path, **kwargs)

    @property
    def products(self):
        return self.get('products').json()

    @property
    def packages(self):
        return self.get('packages').json()

    def create_package(self, name='name', description='desc', products=None):
        product_ids = products or []
        payload = {
            'name': name,
            'description': description,
            'productResources': [{'id': i} for i in product_ids]
        }
        return self.post('packages', json=payload).json()

    def cleanup(self):
        for package in self.packages:
            self.delete('packages/{}'.format(package['id']))


@pytest.fixture
def api(request):
    api_root = request.config.getoption('--api-root')
    api_wrapper = PackagesAPI(api_root)
    yield api_wrapper
    api_wrapper.cleanup()
