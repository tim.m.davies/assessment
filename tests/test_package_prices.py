import pytest


def test_usd_price_no_products_check_price(api):
    created_package = api.create_package()
    response = api.get('packages/{}'.format(created_package['id']))
    assert response.status_code == 200
    package = response.json()
    assert package['totalPrice'] == 0


def test_usd_price_single_product(api):
    product = api.products[0]
    created_package = api.create_package(products=[product['id']])
    response = api.get('packages/{}'.format(created_package['id']))
    assert response.status_code == 200
    package = response.json()
    assert package['totalPrice'] == product['usdPrice']


def test_usd_price_repeated_product(api):
    product = api.products[0]
    created_package = api.create_package(products=[product['id'], product['id']])
    response = api.get('packages/{}'.format(created_package['id']))
    assert response.status_code == 200
    package = response.json()
    assert package['totalPrice'] == product['usdPrice'] * 2


def test_usd_price_multiple_products(api):
    products = api.products
    selected_products = [products[1], products[3], products[5]]
    created_package = api.create_package(products=[p['id'] for p in selected_products])
    response = api.get('packages/{}'.format(created_package['id']))
    assert response.status_code == 200
    package = response.json()
    expected_price = sum(p['usdPrice'] for p in selected_products)
    assert package['totalPrice'] == expected_price


def test_usd_price_single_product_explicit_currency(api):
    product = api.products[0]
    created_package = api.create_package(products=[product['id']])
    response = api.get('packages/{}?currency=USD'.format(created_package['id']))
    assert response.status_code == 200
    package = response.json()
    assert package['totalPrice'] == product['usdPrice']


@pytest.mark.parametrize('currency', ['EUR', 'GBP', 'JPY'])
def test_valid_currency_prices_check_not_usd_price(api, currency):
    product = api.products[0]
    created_package = api.create_package(products=[product['id']])
    response = api.get('packages/{}?currency={}'.format(created_package['id'], currency))
    assert response.status_code == 200
    package = response.json()
    assert package['totalPrice'] != product['usdPrice']


@pytest.mark.xfail
@pytest.mark.parametrize('currency', ['eur', 'YEN', 123])
def test_invalid_currency_prices_check_response_code(api, currency):
    product = api.products[0]
    created_package = api.create_package(products=[product['id']])
    response = api.get('packages/{}?currency={}'.format(created_package['id'], currency))
    assert 400 <= response.status_code < 500
