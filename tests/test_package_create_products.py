import pytest


def test_create_package_one_existing_product_check_response(api):
    products = api.products
    payload = {
        'name': 'name',
        'description': 'desc',
        'productResources': [{
            'id': products[0]['id']
        }]
    }
    response = api.post('packages', json=payload)
    assert response.status_code == 200
    assert response.json()['productResources'] == products[0:1]


def test_create_package_unknown_product_check_response(api):
    payload = {
        'name': 'name',
        'description': 'desc',
        'productResources': [{'id': 'foobar'}]
    }
    response = api.post('packages', json=payload)
    assert response.status_code == 404
    products = api.get('packages')
    assert len(products.json()) == 0


def test_create_package_two_products(api):
    products = api.products
    payload = {
        'name': 'name',
        'description': 'desc',
        'productResources': [
            {'id': products[0]['id']},
            {'id': products[1]['id']},
        ]
    }
    response = api.post('packages', json=payload)
    assert response.status_code == 200
    assert response.json()['productResources'] == products[0:2]


def test_create_package_one_product_unknown(api):
    products = api.products
    payload = {
        'name': 'name',
        'description': 'desc',
        'productResources': [
            {'id': products[0]['id']},
            {'id': 'foobar'},
        ]
    }
    response = api.post('packages', json=payload)
    assert response.status_code == 404
    products = api.get('packages')
    assert len(products.json()) == 0


def test_create_package_same_product_twice_check_response(api):
    products = api.products
    payload = {
        'name': 'name',
        'description': 'desc',
        'productResources': [{
            'id': products[0]['id']
        },{
            'id': products[0]['id']
        }]
    }
    response = api.post('packages', json=payload)
    assert response.status_code == 200
    assert response.json()['productResources'] == [products[0]] * 2


def test_create_package_with_correct_price(api):
    product = api.products[0]
    payload = {
        'name': 'name',
        'description': 'desc',
        'productResources': [{'id': product['id']}],
        'totalPrice': product['usdPrice']
    }
    response = api.post('packages', json=payload)
    assert response.status_code == 200
    assert response.json()['totalPrice'] == product['usdPrice']


@pytest.mark.xfail
def test_create_package_with_incorrect_price(api):
    product = api.products[0]
    payload = {
        'name': 'name',
        'description': 'desc',
        'productResources': [{'id': product['id']}],
        'totalPrice': product['usdPrice'] + 10
    }
    response = api.post('packages', json=payload)
    assert response.status_code == 400
