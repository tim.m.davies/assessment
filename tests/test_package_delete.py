
def test_delete_with_existing_package(api):
    package = api.create_package()
    response = api.delete('packages/{}'.format(package['id']))
    assert response.status_code == 200
    assert len(api.packages) == 0


def test_delete_with_multiple_packages(api):
    delete_package = api.create_package()
    remaining_package = api.create_package()
    response = api.delete('packages/{}'.format(delete_package['id']))
    assert response.status_code == 200
    assert api.packages == [remaining_package]


def test_delete_unknown_package(api):
    response = api.delete('packages/foobar')
    assert response.status_code == 400
    assert len(api.packages) == 0
