# -*- coding: utf-8 -*-

import pytest


class ValidPayloadMixin(object):
    parameter = ''

    def test_create_check_status_code(self, api, payload):
        response = api.post('packages', json=payload)
        assert response.status_code == 200

    def test_create_check_one_package_created(self, api, payload):
        api.post('packages', json=payload)
        all_packages = api.get('packages').json()
        assert len(all_packages) == 1

    def test_create_check_response_data(self, api, payload):
        response = api.post('packages', json=payload)
        assert response.json()[self.parameter] == payload[self.parameter]

    def test_create_check_package_created(self, api, payload):
        response = api.post('packages', json=payload)
        package_id = response.json()['id']
        created_package = api.get('packages/{}'.format(package_id)).json()
        assert created_package[self.parameter] == payload[self.parameter]


class InvalidPayloadMixin(object):

    def test_create_check_status_code(self, api, payload):
        response = api.post('packages', json=payload)
        # Expect some sort of client error in all cases
        assert 400 <= response.status_code < 500

    def test_create_check_no_package_created(self, api, payload):
        api.post('packages', json=payload)
        packages = api.get('packages').json()
        assert len(packages) == 0


class TestValidPackageNames(ValidPayloadMixin):
    parameter = 'name'

    @pytest.fixture(params=[
        'a',
        'outfit',
        'OUTFIT',
        'n' * 100,
        'café',
    ])
    def payload(self, request):
        yield {
            'name': request.param,
            'description': 'desc',
            'productResources': []
        }


class TestValidPackageDescriptions(ValidPayloadMixin):
    parameter = 'description'

    @pytest.fixture(params=[
        'd',
        'A fine outfit',
        'CLOTHES',
        'd' * 250,
        'Survives 180°C',
    ])
    def payload(self, request):
        yield {
            'name': 'name',
            'description': request.param,
            'productResources': []
        }


@pytest.mark.xfail
class TestInvalidButAcceptedPackageNames(InvalidPayloadMixin):

    @pytest.fixture(params=[
        '',
        1,
        '\x08',  # ASCII backspace
    ])
    def payload(self, request):
        yield {
            'name': request.param,
            'description': 'desc',
            'productResources': []
        }


@pytest.mark.xfail
class TestOverlyLongPackageNames(InvalidPayloadMixin):

    # Server generates 500 errors for names longer than 255 characters
    @pytest.fixture(params=[101, 200, 255, 256, 300, 400, 500])
    def payload(self, request):
        yield {
            'name': 'a' * request.param,
            'description': 'desc',
            'productResources': []
        }


@pytest.mark.xfail
class TestInvalidButAcceptedPackageDescriptions(InvalidPayloadMixin):

    @pytest.fixture(params=[
        '',
        1,
        '\x08',  # ASCII backspace
    ])
    def payload(self, request):
        yield {
            'name': 'name',
            'description': request.param,
            'productResources': []
        }


@pytest.mark.xfail
class TestOverlyLongPackageDescriptions(InvalidPayloadMixin):

    # Server generates 500 errors for descriptions longer than 255 characters
    @pytest.fixture(params=[201, 255, 256, 300, 400, 500])
    def payload(self, request):
        yield {
            'name': 'name',
            'description': 'a' * request.param,
            'productResources': []
        }


@pytest.mark.xfail
class TestMissingParameter(InvalidPayloadMixin):

    @pytest.fixture(params=['name', 'description', 'productResources'])
    def payload(self, request):
        payload = {
            'name': 'name',
            'description': 'desc',
            'productResources': []
        }
        del(payload[request.param])
        yield payload


@pytest.mark.xfail
class TestDuplicateNames(object):

    @pytest.fixture
    def first_package(self, api):
        product = api.products[0]
        payload = {
            'name': 'repeated name',
            'description': 'Thing One',
            'productResources': [{'id': product['id']}]
        }
        yield api.post('packages', json=payload)

    @pytest.fixture
    def second_package(self, api):
        product = api.products[1]
        payload = {
            'name': 'repeated name',
            'description': 'Thing Two',
            'productResources': [{'id': product['id']}]
        }
        yield api.post('packages', json=payload)

    def test_second_package_response_code(self, first_package, second_package):
        assert 400 <= second_package.response_code < 500

    def test_one_package_created(self, api, first_package, second_package):
        assert len(api.packages) == 1
