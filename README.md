# Assesment Application Test Suite

## Installation

The tests depend on Python 3 and the packages `requests` and `pytest`, available from PyPI.

Installing a suitable python and pip should be possible on Mac OS with:

    brew install python

Following this, the packages can be installed with:


    pip install -r requirements.txt


## Running the Tests

The tests can then be run with the pytest runner:


    pytest tests


This assumes the target application is running at the default location `http://localhost:50080/api/v1/`

If this is not the case, an alternative URL base can be provided with the `--api-root` parameter:

    pytest tests --api-root http://example.com:8080/api/v1/